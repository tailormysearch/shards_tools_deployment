##
# Shards Toolkit Deployment
# 
# Troposphere script to build deploy the full MS stack to AWS
##

# Constants #

import src.BeanstalkGenerator as BSG
import src.ElasticsearchGenerator as ESG

# Main Function #

def main():
    templates = []
    # TODO get s3 bucket
    
    # Generate ES service
    templates.append(ESG.generateMicro("products", "product"))
    # TODO generate 5 BS instances

    # Concatenate all templates to one file
    json_dump = ""
    for template in templates:
        json_dump += template.to_json()
    print(json_dump)

if __name__ == "__main__":
    main()