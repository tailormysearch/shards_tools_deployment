##
# Shards Toolkit Deployment - eanstalk instance CLI 
# 
# Troposphere script to build deploy the full MS stack to AWS
##

import sys
import src.BeanstalkGenerator as BSG

# Constants #

keys_file = "keys.txt"

# Main Function #

def main():
    with open(keys_file) as f:
        keys = f.readlines()
    keys = [x.strip() for x in keys] 

    if len(sys.argv) > 5 or len(sys.argv) < 4:
        print("wrong usage: <name> <description> <bucket>, and optional <bucket_key>")
    else:
        bucket_key = None
        if len(sys.argv) == 5:
            bucket_key = sys.argv[4]
        template = BSG.generate(
            sys.argv[1],
            description=sys.argv[2],
            bucket=sys.argv[3],
            bucket_key=bucket_key
        )
        print(template)

if __name__ == "__main__":
    main()