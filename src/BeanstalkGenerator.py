from troposphere import (
    GetAtt, Join, Output,
    Parameter, Ref, Template, FindInMap
)

from troposphere.elasticbeanstalk import (
    Application, ApplicationVersion, ConfigurationTemplate, Environment,
    SourceBundle, OptionSettings
)

from troposphere.iam import Role, InstanceProfile
from troposphere.iam import PolicyType as IAMPolicy

from awacs.aws import Allow, Statement, Action, Principal, Policy
from awacs.sts import AssumeRole

# beanstalk template generator 

def generate(name, bucket, description="Sample Description", bucket_key=None):
    if bucket_key == None:
        bucket_key = name
    environment_name = name+"Env"
    version_name = name+"Version"
    config_name = name+"ConfigurationTemplate"

    t = Template(Description=name+": "+description)
    t.add_version()

    keyname = t.add_parameter(Parameter(
        "KeyName",
        Description="C2 KeyPair to enable SSH access to the instance",
        Type="AWS::EC2::KeyPair::KeyName",
        ConstraintDescription="must be the name of an existing EC2 KeyPair."
    ))

    t.add_mapping("Regional2Principal", {
        'eu-west-1': {
            'EC2Principal': 'ec2.amazonaws.com',
            'OpsWorksPrincipal': 'opsworks.amazonaws.com'}
    })

    t.add_resource(Role(
        "WebServerRole",
        AssumeRolePolicyDocument=Policy(
            Statement=[ Statement(
                    Effect=Allow, Action=[AssumeRole],
                    Principal=Principal(
                        "Service", [
                            FindInMap(
                                "Region2Principal",
                                Ref("AWS::Region"), "EC2Principal")
                        ]
                    )
                )
            ]
        ),
        Path="/"
    ))

    t.add_resource(IAMPolicy(
        "WebServerRolePolicy",
        PolicyName="WebServerRole",
        PolicyDocument=Policy(
            Statement=[
                Statement(Effect=Allow, NotAction=Action("iam", "*"),
                        Resource=["*"])
            ]
        ),
        Roles=[Ref("WebServerRole")]
    ))

    t.add_resource(InstanceProfile(
        "WebServerInstanceProfile",
        Path="/",
        Roles=[Ref("WebServerRole")]
    ))

    t.add_resource(Application(name, Description=description))

    t.add_resource(ApplicationVersion(
        version_name,
        Description="Version 0.1",
        ApplicationName=Ref(name),
        SourceBundle=SourceBundle(
            S3Bucket=Join("-", [bucket, Ref("AWS::Region")]),
            S3Key=bucket_key
        )
    ))

    t.add_resource(ConfigurationTemplate(
        config_name,
        ApplicationName=Ref(name),
        Description="SSH access to Tomcat Application",
        SolutionStackName="64bit Amazon Linux 2014.03 v1.0.9 running Tomcat",
        OptionSettings=[
            OptionSettings(
                Namespace="aws:autoscaling:launchconfiguration",
                OptionName="EC2KeyName",
                Value=Ref("KeyName")
            ),
            OptionSettings(
                Namespace="aws:autoscaling:launchconfiguration",
                OptionName="IamInstanceProfile",
                Value=Ref("WebServerInstanceProfile")
            )
        ]
    ))

    t.add_resource(Environment(
        environment_name,
        Description="AWS Elastic Beanstalk Environment running Tomcat "
                    "Application",
        ApplicationName=Ref(name),
        TemplateName=Ref(config_name),
        VersionLabel=Ref(version_name)
    ))

    t.add_output(
        Output(
            "URL",
            Description="URL of the AWS Elastic Beanstalk Environment",
            Value=Join("", ["http://", GetAtt(environment_name, "EndpointURL")])
        )
    )

    return t