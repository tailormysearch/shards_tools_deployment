from troposphere import Template, constants
from troposphere.elasticsearch import Domain, EBSOptions
from troposphere.elasticsearch import ElasticsearchClusterConfig
from troposphere.elasticsearch import SnapshotOptions

# Elasticsearch service generators

# TODO change access policy

def generateMicro(domain, description):
    t = Template(Description=description)

    es_domain = t.add_resource(Domain(
        'ElasticsearchDomain',
        DomainName=domain,
        ElasticsearchClusterConfig=ElasticsearchClusterConfig(
            DedicatedMasterEnabled=True,
            InstanceCount=1,
            ZoneAwarenessEnabled=True,
            InstanceType=constants.ELASTICSEARCH_T2_MICRO,
            DedicatedMasterType=constants.ELASTICSEARCH_T2_MICRO,
            DedicatedMasterCount=1
        ),
        EBSOptions=EBSOptions(EBSEnabled=True,
                            Iops=0,
                            VolumeSize=10,
                            VolumeType="gp2"),
        SnapshotOptions=SnapshotOptions(AutomatedSnapshotStartHour=0),
        AccessPolicies={'Version': '2012-10-17',
                        'Statement': [{
                            'Effect': 'Allow',
                            'Principal': {
                                'AWS': '*'
                            },
                            'Action': 'es:*',
                            'Resource': '*'
                        }]},
        AdvancedOptions={"rest.action.multi.allow_explicit_index": "true"}
    ))

    print(t.to_json)
    return t
    